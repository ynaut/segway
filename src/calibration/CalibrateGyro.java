package calibration;


import lejos.nxt.Button;
import lejos.nxt.SensorPort;
import lejos.nxt.addon.GyroSensor;

//Calibrerar driften p� Gyron
public class CalibrateGyro extends Thread  {

	
	public void run(){
		GyroSensor gyro = new GyroSensor(SensorPort.S1,598);
		System.out.println("Gyro: " + gyro.readValue());
		Button.waitForAnyPress(1000);
		double drift = 0;
		long h = 50;
		long t = System.currentTimeMillis();
		long duration = 0;
		for(int i=0; i <1000; i++){
			drift = drift + gyro.readValue();
			t += h;
			duration = t - System.currentTimeMillis();
			if(duration>0){
				try{
					sleep(duration);
				} catch(InterruptedException e){
					// Do nothing
				}
			}	
		}
		drift = drift/1000;
		System.out.println("drift:" + Double.toString(drift));
		Button.waitForAnyPress();
		
	}
}
