

public class Kalman {
	protected double[][] A, B;
	protected double[][] X0; 
	
	public Kalman(double[][] A, double[][] B){	
		this.A = A;
		this.B = B;
		double[][] X0 = {{0},{0},{0},{0}};
		this.X0 = X0;
	}
	
	public double[][] update(double U, double[][] Y){
		double[][] U_new = { {U},{Y[0][0]},{Y[1][0]},{Y[2][0]},{Y[3][0]} };
		for(int i = 0; i < 4; i++){
			X0[i][0] = A[i][0] * X0[0][0] + A[i][1] * X0[1][0] + A[i][2] * X0[2][0] + A[i][3] * X0[3][0];
			X0[i][0] = X0[i][0] + B[i][0] * U_new[0][0] + B[i][1] * U_new[1][0] + B[i][2] * U_new[2][0]+ B[i][3] * U_new[3][0] +B[i][4]*U_new[4][0];	
		}
		return X0;
	}
}