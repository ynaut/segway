
import lejos.nxt.addon.AccelHTSensor;
import lejos.nxt.addon.GyroSensor;
import lejos.nxt.comm.BTConnection;
import lejos.nxt.comm.Bluetooth;

import java.io.DataInputStream;
import java.io.DataOutputStream;

import lejos.nxt.*;

public class Segway {
	double[][] A,B,L;
	LQR lqr;
	PID pid;
	Kalman kalman;
	GyroSensor gyro = new GyroSensor(SensorPort.S1,598);
	AccelHTSensor accel = new AccelHTSensor(SensorPort.S2);
	NXTMotor motorA = new NXTMotor(MotorPort.A);
	NXTMotor motorB = new NXTMotor(MotorPort.B);
	double[] ref = {2,0}; // Need to be synchronized
	BTConnection btc = null;
	DataOutputStream dos = null;
	DataInputStream dis = null;
	double tacho;
	double tachoOld;
	double sampleRate = 0.04; //50 ms
	final double angularOffset = 2.8;
	final double filter = 0.51;
	double [] yOld = {0,0,0};
	double kp = 3, kd = 0.1, ti = 10;
	double angle = 0.0;
	double complementary = 0.8;
	double limit = 100;
	double SCALE = 1;
	double I = 0;
	
	public Segway(){
	    System.out.println("Waiting for BT");
		btc = Bluetooth.waitForConnection();
		dis = btc.openDataInputStream();
		dos = btc.openDataOutputStream();
	    System.out.println("Connected");
	    motorA.resetTachoCount();
	    motorB.resetTachoCount();

	    double[][] KalA = {{0.0097737,0.017007,-2.3698e-05,-0.0015264},{-0.73985,0.51796,-0.0007083,-0.042793},{3.4357e-05,-2.2912e-05,0.037054,0.0018417},{0.00030528,-0.00020153,-0.00029779,0.030859}};	
	    double[][] KalB = {{-0.050974,1.0657,0.034246,2.3698e-05,0.0061604},{-1.7817,3.7982,0.55756,0.0007083,0.20477},{0.20233,-3.4357e-05,2.2912e-05,0.96295,0.029764},{6.9533,-0.00030528,0.00020153,0.00029779,0.33702}}; 
	 
	    double[][] LQR = {{-19.7833,-2.5823,-1.1482,-0.51049}};
		A = KalA;
		B = KalB;
		L = LQR;
		pid = new PID();
		lqr = new LQR(L);
		kalman = new Kalman(A,B);
	}
	
	public double [] getMeasurement(){
		double [] Y = new double [2];
		double anglerate = gyro.readValue();
		double accelAngle = Math.toDegrees(Math.atan2(accel.getXAccel(),accel.getZAccel())) - 90 ;
		angle = (angle + anglerate * sampleRate) *( 1- complementary) + accelAngle * complementary  ;
//		System.out.println("a: "+angle);
		Y[0] = angle;
		Y[1] = anglerate;
		return Y ;
	}
	
	public Segway(double[][] A, double[][] B, double[][] L){
		this.A = A;
		this.B = B;
		this.L = L;
		lqr = new LQR(L);
		kalman = new Kalman(A,B);
	}
	public double lowPassFilter(double measurement, double oldMeasurement){
		double result = (1- filter)* measurement + filter * oldMeasurement;
		return result;
	}
	public double PD(double angle, double velocity){
		double T =  (kp * angle + kd * velocity)/SCALE;
		if( T > limit){
			T = limit;
		}else if(T < -limit){
			T = -limit;
		}
		return T;
	}
	public double PID(double angle ,double velocity){
		double P = angle * kp;
		double D = kp * kd * velocity;
		I = I + kp/ ti * sampleRate*angle;
		double result = P + I+D;
		if( result > limit){
			result = limit;
		}else if(result < -limit){
			result = -limit;
		}
		return result;
	}
	
	
	

}
