import lejos.nxt.Button;
import lejos.nxt.ButtonListener;
import lejos.nxt.Sound;

public class Main {

	public static void main(String[] args) {
		Main main = new Main();
		main.run();
	}
	void run(){
	    Button.ESCAPE.addButtonListener(new ButtonListener() {
	      public void buttonPressed(Button b) {
	    	  System.exit(0);
	      }
	      public void buttonReleased(Button b) {
	        System.exit(0);
	      }
	    });	  
		Segway segway = new Segway();
		//Segway segwayDiffConst = new Segway(null,null,null);
		Balance balance = new Balance(segway);
		Button.ENTER.waitForPressAndRelease();
	    Sound.playTone (1000, 1);
		balance.start();
	}
	
}

