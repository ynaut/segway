public class PID {
	double h = 0.05;
	double Kx1 = 1;
	double Kx2 = 1;
	double Kx3 = 1;
	double Kx4 = 1;
	
	double KIx1 = h;
	double KIx2 = h;
	double KIx3 = h;
	double KIx4 = h;
	
	double Kd1 = 1/h;
	double Kd2 = 1/h;
	double Kd3 = 1/h;
	double Kd4 = 1/h;
	
	double P = 0.0, I = 0.0, D = 0.0;
	double Ix1 = 0.0;
	double Ix2 = 0.0;
	double Ix3 = 0.0;
	double Ix4 = 0.0;
	double x1old=0;
	double x2old=0;
	double x3old=0;
	double x4old=0;
	double scalingFactor = 0.1;
	


	public PID(){
	}

	public double update(double[][] x){

		P = Kx1*x[0][0] + Kx2*x[1][0] + Kx3*x[2][0] + Kx4*x[3][0];
		I = integrate(x);
		D = derivative(x);
		return P +I+D;
	}

	private double integrate(double[][] x){
		Ix1 = (Ix1 + x[0][0]);
		Ix2 = (Ix2 + x[1][0]);
		Ix3 = (Ix3 + x[2][0]);
		Ix4 = (Ix4 + x[3][0]);
	//	return Ix1 * KIx1;
		return (Ix1 * KIx1 + Ix2* KIx2 + Ix3* KIx3 + Ix4 * KIx4);

	}
	private double derivative(double [][] x){
		double d1 = x[0][0] - x1old;
		double d2 = x[1][0] - x2old;
		double d3 = x[2][0] - x2old;
		double d4 = x[3][0] - x2old;
		x1old = x[0][0];
		x2old = x[1][0];
		x3old = x[2][0];
		x4old = x[3][0];
		return (d1 * Kd1+ d2 * Kd2 + d3 * Kd3 + d4 *Kd4 );
	}
}



//kAng*angle + kAngRate*rate + kIntAng*intAngle + kPos*pos + kPosRate*speed;