

public class LQR {
	protected double[][] L;
	protected final double limit = 100;
	protected final double motorScaling = 0.205;
	public LQR(double[][] L){
		this.L = L;
	}
	
	public double update(double[] X){
		//double returnValue = L[0][0] * X[0][0] + L[0][1] * X[1][0] + L[0][2] * X[2][0]+L[0][3] * X[3][0];
		double returnValue = 0;
		returnValue = -returnValue;
		returnValue = returnValue*motorScaling;
		if(returnValue > limit){
			return returnValue = limit;
		} else if(returnValue < -limit){
			return -limit;
		} else{
			return returnValue;
		}

	}
}
