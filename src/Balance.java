import java.io.IOException;


public class Balance extends Thread{
	Segway segway;
	double[] E = new double [2];
	double uold = 0.0;
	double[] Y;
	double u;
	public Balance(Segway segway){
		this.segway = segway;
	}

	public void run(){
		System.out.println("In Balance");
		long h = 40;
		long t = System.currentTimeMillis();
		long duration;
		while(true){
			synchronized(segway){
				Y = segway.getMeasurement();
				double[] ref = segway.ref;
				E[0] = ref[0] - Y[0];
				E[1] = ref[1] - Y[1];
				u = segway.PID(E[0], E[1]);
				int power = (int) Math.floor(u);
				segway.motorA.setPower(power);
				segway.motorB.setPower(power);
			}
			uold = u;
			//Reading new Parameters from terminal
			try{
				//Checks if the input is on the right format
				if(segway.dis.available() > 8){
					String input = segway.dis.readUTF();
					if(!input.equals(null)){
					double kp = Double.valueOf(input.substring(0,3));
					double kd = Double.valueOf(input.substring(3,6));
					double ti = Double.valueOf(input.substring(6,9));
					segway.kp = kp;
					segway.kd = kd;
					segway.ti=ti;
					}
				}
			}catch (IOException e1) {
				e1.printStackTrace();
			}
			//Writing new parameters
			try {
				segway.dos.writeDouble(u);
				segway.dos.flush();
				segway.dos.writeUTF( Double.toString(Y[0]) + ":" + Double.toString(Y[1]) ) ;
				segway.dos.flush();
				segway.dos.writeUTF( Double.toString(E[0]) + ":" + Double.toString(E[1])) ;
				segway.dos.flush();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			t += h;
			duration = t - System.currentTimeMillis();
			if(duration>0){
				try{
					sleep(duration);
				} catch(InterruptedException e){
					// Do nothing
				}
			}

		}
	}
}


